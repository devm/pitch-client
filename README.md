# 广州摆摊地图

#### 介绍
广州最新摆摊点，目前广州已开放60个临时摊贩疏导区

#### 软件架构
使用uniapp开发，目前已接入测试服务器数据和uniCloud数据，可编码成各个版本


#### 使用说明

1.  在index.vue -> data 中 fromUniCloud：true， 表示，使用uniCloud数据请求，否则将请求测试服务器数据。
2.  使用uniCloud数据时，需要先根据uniCloud文档创建服务空间，[uniCloud教程](https://uniapp.dcloud.net.cn/uniCloud/README)
3.  右键db_init.json进行数据初始化，导入到云数据库。（PS：如初始化失败，请先登录到[ 云数据库 ](https://unicloud.dcloud.net.cn/cloud-database)创建云数据表 pitch ）

![输入图片说明](https://images.gitee.com/uploads/images/2020/0608/220701_37bb637d_7372206.png "微信图片_20200608220631.png")

#### 讨论交流
加入QQ群讨论：455892987

#### 扫描体验
![输入图片说明](https://images.gitee.com/uploads/images/2020/0609/155553_df7a31e7_7372206.jpeg "gh_c133f81e1d12_430.jpg")