'use strict';
const db = uniCloud.database()

exports.main = async (event, context) => {
  //event为客户端上传的参数
  console.log('event : ' + event)
  
  const coll = db.collection('pitch')
  
  const limit = 10;
  var page = event.page;
  
  var res = await coll.skip((page-1)*10)
  if(event.kw) {
	   const dbCmd = db.command
	   res = res.where(
		dbCmd.or({
			pitchAddress:new RegExp(event.kw)
		},{
			scopeBusiness:new RegExp(event.kw)
		})
	  )
  }
  res = res.limit(limit).get()
  console.log(res)
  //返回数据给客户端
  return res
};
