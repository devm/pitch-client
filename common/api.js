import util from './util.js'

// const apibath = 'http://192.168.101.20:8802/' //API根路径
const apibath = 'https://wx.treetao.com' //API根路径

const pitchlist = (page, kw) => {
	return util.get(apibath + '/mobile/pitch/list', {
		p: page,
		kw:kw
	});
}

export default {
	pitchlist 
}
